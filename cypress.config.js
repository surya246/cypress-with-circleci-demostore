const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    chromeWebSecurity: false,
    defaultCommandTimeout: 12000,
    pageLoadTimeout: 80000,
    env: {
        url: 'https://demostore.x-cart.com/',
    },
    retries: {
            runMode: 2,
    },
    specPattern: 'cypress/e2e/specs/*.spec.js',

    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
