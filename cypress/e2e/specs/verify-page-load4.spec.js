/// <reference types="Cypress" />


describe('load page',()=>{

    it('Should be able load page',()=>{
      cy.visit(Cypress.env('url'))
        cy.url().should('eq',"https://demostore.x-cart.com/")
    })

})
